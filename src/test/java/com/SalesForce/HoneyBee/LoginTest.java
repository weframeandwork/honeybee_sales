package com.SalesForce.HoneyBee;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.Login;

public class LoginTest extends Base{
	
	public Login t1;
	
	@BeforeTest
	public void getDriver() throws IOException
	{
		driver=initializeDriver();
		driver.get(prop.getProperty("HostUrl"));
		t1=new Login(driver);
		
	}
	
	
	@Test
	public void EnterCredentials() throws IOException
	{
		t1=new Login(driver);
		
		t1.getUsername().sendKeys("username");
		t1.getPassword().sendKeys("password");
		t1.clickLogin().click();
		
		String title=driver.getTitle();
		
		System.out.println(title);
		
		//Assert.assertEquals(actual, expected);
		
		
		
	}
	
	@AfterTest(enabled=false)
	public void tearDown()
	{
		driver.quit();
	}

}
