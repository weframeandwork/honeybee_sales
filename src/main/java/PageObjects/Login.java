package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login {
	
	WebDriver driver;
	
	
	public Login(WebDriver driver)
	{
		this.driver=driver;
	}
	
	// All locators related to Login Page
	
	By username=By.id("username");
	By password=By.id("password");
	By loginButton=By.id("Login");
	
	
	//All Methods Available in Login Page
	
	public WebElement getUsername()
	{
		return driver.findElement(username);
	}
	
	public WebElement getPassword()
	{
		return driver.findElement(password);
	}
	
	public WebElement clickLogin()
	{
		return driver.findElement(loginButton);
	}

}
