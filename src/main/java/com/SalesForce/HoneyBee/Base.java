package com.SalesForce.HoneyBee;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Base {

 public WebDriver driver;
 Properties prop;

    public WebDriver initializeDriver() throws IOException
    {
    	
    	FileInputStream file=new FileInputStream("C:\\Users\\PSK Enterprises\\Desktop\\selenium\\HoneyBee\\src\\main\\java\\Resources\\data.properties");
    	
    	prop=new Properties();
    	
    	prop.load(file);
    	
    	 String browsername=prop.getProperty("browser1");
    	if(browsername.equalsIgnoreCase("Chrome"))
    	{
    		
    		WebDriverManager.chromedriver().setup();
    		
    		driver=new ChromeDriver();
    		
    	}
    	
    	else if(browsername.equalsIgnoreCase("firefox"))
    	{
    		WebDriverManager.firefoxdriver().setup();
    		driver=new FirefoxDriver();
    	}
    	
    	else if(browsername.equalsIgnoreCase("InternetExplorer"))
    	{
    		WebDriverManager.iedriver().setup();
    		
    		driver=new InternetExplorerDriver();
    	}
    	
    	else
    	{
    		WebDriverManager.edgedriver().setup();
    		driver=new SafariDriver();
    	}
    	
    	driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
    	
    	return driver;
    			
    }
}
